<?php
/**
 * As configurações básicas do WordPress
 *
 * O script de criação wp-config.php usa esse arquivo durante a instalação.
 * Você não precisa usar o site, você pode copiar este arquivo
 * para "wp-config.php" e preencher os valores.
 *
 * Este arquivo contém as seguintes configurações:
 *
 * * Configurações do MySQL
 * * Chaves secretas
 * * Prefixo do banco de dados
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/pt-br:Editando_wp-config.php
 *
 * @package WordPress
 */

// ** Configurações do MySQL - Você pode pegar estas informações com o serviço de hospedagem ** //
/** O nome do banco de dados do WordPress */
define('DB_NAME', 'imobiliaria');

/** Usuário do banco de dados MySQL */
define('DB_USER', 'root');

/** Senha do banco de dados MySQL */
define('DB_PASSWORD', '');

/** Nome do host do MySQL */
define('DB_HOST', 'localhost');

/** Charset do banco de dados a ser usado na criação das tabelas. */
define('DB_CHARSET', 'utf8mb4');

/** O tipo de Collate do banco de dados. Não altere isso se tiver dúvidas. */
define('DB_COLLATE', '');

/**#@+
 * Chaves únicas de autenticação e salts.
 *
 * Altere cada chave para um frase única!
 * Você pode gerá-las
 * usando o {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org
 * secret-key service}
 * Você pode alterá-las a qualquer momento para invalidar quaisquer
 * cookies existentes. Isto irá forçar todos os
 * usuários a fazerem login novamente.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'm?G+I.?W,!396zFYMb?pQ?qFmVmRn>9YxkA1Q_ZU,K/3/4eHImDNZWa#]}&{eE=#');
define('SECURE_AUTH_KEY',  'i~7ggb6R0%wT]k?^prnRkO$9in4JZ7Hd)Vqpb>zt*tDI=Kug`0U+(YaMyAB:jdkN');
define('LOGGED_IN_KEY',    ',z)lm3P;xjMyM1Pir4N0%Dt~}TFj>;IUpQczfL!-4d+7BqD%B3w.#bYa~ggACl?&');
define('NONCE_KEY',        '~f9j~hoC:I3z.#FBc*W_/~wfBM9Gh!%R.k^zDK&P9d Ie2agf`}]Hj@ KcYyy ]L');
define('AUTH_SALT',        '?ur56U!Eupi5Pz{X0q@+<[`s(TS6/k^3y b[Q_r $N7o=;Eb9$xMryz</4f5![]R');
define('SECURE_AUTH_SALT', 'zVFLB1IA)Jq_bjU.Nxz}<@)+EMB,kIo<2(.!3+ 4p1Rvdf#kxl,( <~Iq{,.T3y(');
define('LOGGED_IN_SALT',   'k4`KF#]qgsq6-N8]sWJb*_V_Tl0)<,AWvJ,/1[,eaalPnKY(Bo*Aq5*W!:jT:5,B');
define('NONCE_SALT',       'QT(p4%QEy7r`TK5X4(&OuCaI-/z{>2h%H=&n3Rz[_w4zlB* )y!-nB%OIT8As{C;');

/**#@-*/

/**
 * Prefixo da tabela do banco de dados do WordPress.
 *
 * Você pode ter várias instalações em um único banco de dados se você der
 * um prefixo único para cada um. Somente números, letras e sublinhados!
 */
$table_prefix  = 'wp_';

/**
 * Para desenvolvedores: Modo de debug do WordPress.
 *
 * Altere isto para true para ativar a exibição de avisos
 * durante o desenvolvimento. É altamente recomendável que os
 * desenvolvedores de plugins e temas usem o WP_DEBUG
 * em seus ambientes de desenvolvimento.
 *
 * Para informações sobre outras constantes que podem ser utilizadas
 * para depuração, visite o Codex.
 *
 * @link https://codex.wordpress.org/pt-br:Depura%C3%A7%C3%A3o_no_WordPress
 */
define('WP_DEBUG', false);

/* Isto é tudo, pode parar de editar! :) */

/** Caminho absoluto para o diretório WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Configura as variáveis e arquivos do WordPress. */
require_once(ABSPATH . 'wp-settings.php');
